// This is a template and apart from what we need to do

import { app, BrowserWindow } from "electron";
import * as path from "path";

let mainWindow: Electron.BrowserWindow;

function createWindow() {

  mainWindow = new BrowserWindow({
    height: 600,
    width: 800,
    "webPreferences": {
      "webSecurity": false
    }
  });

  mainWindow.loadURL(path.resolve('index.html'))

  mainWindow.on("closed", () => {

    mainWindow = null;
  });
}

app.on("ready", createWindow);
app.on("window-all-closed", () => {

  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {

  if (mainWindow === null) {
    createWindow();
  }
});

