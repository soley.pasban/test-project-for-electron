// This single file should be several files 
// interfaces should be stored into separate files inside interface folder

const data = require('./data/scene.json')

interface ITiming {
    start: number
    duration: number
    end: number
}

interface IElements {
    x: number
    y: number
    visible: number
    timing: ITiming
    updateProps(prop: ISceneData): void
    setPosition(x: number, y: number): void
    play(): void
}

interface IImageElements extends IElements {
    src: string
    image: HTMLImageElement
}

interface ISceneData {
    src: string
    x: number
    y: number
    timing: ITiming
}

class Timing implements ITiming {
    start: number
    duration: number
    end: number

    constructor(
        start: number = 0,
        duration: number = 0,
        end: number = 0
    ) {
        this.start = start
        this.duration = duration
        this.end = end
    }
}

class Elements implements IElements {
    x: number
    y: number
    visible: number
    timing: ITiming

    constructor() {
        this.x = 0
        this.y = 0
        this.visible = 0
        this.timing = new Timing()
    }

    setPosition(x: number, y: number) {
        this.x = x
        this.y = y
    }

    updateProps(prop: ISceneData) {
        this.setPosition(prop.x, prop.y)
        this.timing = prop.timing
    }

    play() {
    }
}

class ImageElement extends Elements implements IImageElements {
    src: string
    image: HTMLImageElement

    constructor(src: string) {
        super()
        this.src = src
        this.image = <HTMLImageElement>document.createElement("IMG");
        this.image.src = src
        this.image.style.position = 'absolute'
        this.image.style.opacity = 'none'
        document.getElementById('container').appendChild(this.image);
    }

    setPosition(x: number, y: number) {
        super.setPosition(x, y)
        this.image.style.left = x + 'px'
        this.image.style.top = y + 'px'
    }

    play() {
        const showImage = () => this.image.style.display = ''
        const hideImage = () => this.image.style.display = 'none'
        hideImage()

        setTimeout(showImage, this.timing.start * 1000)
        setTimeout(hideImage, (this.timing.start + this.timing.duration) * 1000)
        // no need to use --end-- time -> will be used in fade out effect
    }
}

interface IScene {
    sceneItems: ISceneObject[]
    readSceneData(elements: ISceneData[]): void
    animate(): void
}

enum Types {
    ELEMENT,
    IMAGE
}
interface ISceneObject {
    type: Types,
    object: Element | ImageElement
}


class Scene implements IScene {
    sceneItems: ISceneObject[]

    constructor() {
        this.sceneItems = []
    }

    readSceneData(elements: ISceneData[]) {
        elements.forEach(obj => {
            console.log(obj)
            const object: ImageElement = new ImageElement(obj.src)
            object.updateProps(obj)
            this.sceneItems.push({ type: Types.IMAGE, object })
        });
    }

    animate() {
        this.sceneItems.forEach(obj => {
            if (obj.type === Types.IMAGE) (<ImageElement>obj.object).play()
        });
    }
}

const scene: Scene = new Scene()
scene.readSceneData(data)

const startButton = document.createElement("BUTTON");
document.getElementById('container').appendChild(startButton);
startButton.innerHTML = "Start animation"
startButton.onclick = () => scene.animate()
